port module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Chart as C
import Chart.Attributes as CA
import Dict exposing (Dict)
import Element as El
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Html.Events
import Json.Decode as JD
import Json.Encode as JE
import List.Extra exposing (minimumBy)
import Random
import Round
import Set
import Svg exposing (Svg)
import Svg.Attributes
import Task
import Time
import Words exposing (guessWords, secretWords)



-- MAIN


main : Program JD.Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions =
            \model ->
                if hasWonToday model then
                    Time.every 1000 Tick

                else
                    Sub.none
        }



-- MODEL


type alias Model =
    { content : String
    , now : Maybe Time.Posix
    , persistedData : PersistedData
    }


init : JD.Value -> ( Model, Cmd Msg )
init persistedJson =
    let
        data : PersistedData
        data =
            JD.decodeValue decodePersistedData persistedJson
                |> Result.withDefault Dict.empty
    in
    ( { content = ""
      , now = Nothing
      , persistedData = data
      }
    , Task.perform Tick Time.now
    )


{-| True if the user has guessed today's word
-}
hasWonToday : Model -> Bool
hasWonToday model =
    case model.now of
        Just now ->
            model.persistedData
                |> Dict.get (daysSince1970 now)
                |> Maybe.withDefault []
                |> List.any ((==) <| secretWordForTime now)

        Nothing ->
            False


{-| True if the game was won on day number `dayNumber`,
according to PersistedData
-}
hasWonDay : DayNumber -> PersistedData -> Bool
hasWonDay dayNumber data =
    case Dict.get dayNumber data of
        Just guesses ->
            List.any
                ((==) <| secretWordForDay dayNumber)
                guesses

        Nothing ->
            False


{-| Get today's guesses from `PersistedData`
-}
todaysGuesses : Model -> List String
todaysGuesses model =
    case model.now of
        Just now ->
            model.persistedData
                |> Dict.get (daysSince1970 now)
                |> Maybe.withDefault []

        Nothing ->
            []



-- PERSISTED DATA STRUCTURE


{-| PersistedData is a dictionary of daynum: guesses pairs.
-}
type alias PersistedData =
    Dict DayNumber (List String)


type alias DayNumber =
    Int


{-| Use this function to save persisteddata to localstorage
-}
save : PersistedData -> Cmd msg
save =
    encodePersistedData >> persistData


{-| The javascript port used to yeet strings into localstorage
-}
port persistData : String -> Cmd msg


{-| Append a guess to today's list of guesses
-}
addGuess : Time.Posix -> String -> PersistedData -> PersistedData
addGuess now guess data =
    let
        today =
            daysSince1970 now

        newGuessList =
            Dict.get today data
                |> Maybe.withDefault []
                |> (\lst -> guess :: lst)
    in
    Dict.insert today newGuessList data


{-| Decode JSON string into PersistedData dict
-}
decodePersistedData : JD.Decoder PersistedData
decodePersistedData =
    -- decode a (str: list str) dictionary
    JD.dict (JD.list JD.string)
        -- convert that dict to (str: list str)
        |> JD.map
            (Dict.toList
                >> List.filterMap
                    (\( k, v ) -> String.toInt k |> Maybe.map (\i -> ( i, v )))
                >> Dict.fromList
            )


{-| Encode PersistedData dict into JSON string
-}
encodePersistedData : PersistedData -> String
encodePersistedData data =
    data
        |> Dict.toList
        |> List.map (\( k, v ) -> ( String.fromInt k, JE.list JE.string v ))
        |> JE.object
        |> JE.encode 0


{-| Number of days the user has won in a row.
-}
streak : PersistedData -> DayNumber -> Int
streak data dayNumber =
    if hasWonDay dayNumber data then
        1 + streak data (dayNumber - 1)

    else
        0


{-| The average number of guesses used to win
-}
averageGuesses : PersistedData -> Float
averageGuesses data =
    let
        wonDays : List ( DayNumber, List String )
        wonDays =
            data
                |> Dict.toList
                |> List.filter
                    (\( dayNumber, guesses ) ->
                        List.any ((==) (secretWordForDay dayNumber)) guesses
                    )
    in
    wonDays
        |> List.map (Tuple.second >> List.length)
        |> List.sum
        |> (\sum -> toFloat sum / toFloat (List.length wonDays))



-- WORD SELECTION


{-| Given a posix time, return the number of days since 1970
rolls over on midnight in UTC.
-}
daysSince1970 : Time.Posix -> DayNumber
daysSince1970 now =
    Time.posixToMillis now
        |> (\millis -> toFloat millis / (1000 * 60 * 60 * 24))
        |> ceiling


{-| Generator that outputs a random word from `secretWords`
-}
randomSecretWord : Random.Generator String
randomSecretWord =
    Random.uniform "sorry" secretWords


{-| The appropriate secret word for a given timestamp
-}
secretWordForTime : Time.Posix -> String
secretWordForTime =
    daysSince1970 >> secretWordForDay


{-| The appropriate secretword for a given day number
-}
secretWordForDay : DayNumber -> String
secretWordForDay =
    Random.initialSeed >> Random.step randomSecretWord >> Tuple.first



-- UPDATE


type Msg
    = SubmitString String
    | Change String
    | Tick Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick now ->
            -- update current time and secret word
            ( { model | now = Just now }
            , Cmd.none
            )

        Change newContent ->
            -- user edited the text input
            ( { model | content = String.toLower newContent }
            , Cmd.none
            )

        SubmitString content ->
            -- user submitted a guess
            let
                guess =
                    String.toLower content

                guessIsWord =
                    Set.member guess guessWords
            in
            case ( guessIsWord, model.now ) of
                ( True, Just now ) ->
                    -- guess a real word, and we have up-to-date time
                    let
                        newPersistedData =
                            addGuess now guess model.persistedData

                        newModel =
                            { model
                                | content = ""
                                , persistedData = newPersistedData
                            }
                    in
                    ( newModel
                    , save newPersistedData
                    )

                ( _, _ ) ->
                    {- either the guess wasn't a real word
                       or the time data hasn't shown up yet (very unlikely...)
                       do nothing in both cases
                    -}
                    ( model, Cmd.none )



-- VIEW


numGuessesShown : Int
numGuessesShown =
    5


bgColor : El.Color
bgColor =
    El.rgb 1 0.95 0.95


fgColor : El.Color
fgColor =
    El.rgb 0.4 0.3 0.3


noFocusStyle : El.FocusStyle
noFocusStyle =
    { borderColor = Nothing, backgroundColor = Nothing, shadow = Nothing }


view : Model -> Html Msg
view model =
    El.layoutWith
        { options = [ El.focusStyle noFocusStyle ] }
        [ El.width El.fill
        , El.height El.fill
        , Background.color bgColor
        , Font.color fgColor
        ]
    <|
        if hasWonToday model then
            viewWinScreen model

        else
            viewGameScreen model


viewGameScreen : Model -> El.Element Msg
viewGameScreen model =
    let
        guess : String
        guess =
            String.toLower model.content

        guesses : List String
        guesses =
            todaysGuesses model

        secret : String
        secret =
            model.now
                |> Maybe.map secretWordForTime
                |> Maybe.withDefault ""

        guessesAfter : List (El.Element Msg)
        guessesAfter =
            viewGuesses After guesses secret

        guessesBefore : List (El.Element Msg)
        guessesBefore =
            viewGuesses Before guesses secret

        contentIsWord : Bool
        contentIsWord =
            Set.member guess guessWords

        inputField : El.Element Msg
        inputField =
            viewInputField (List.length guesses) guess contentIsWord
    in
    El.column
        [ El.spacing 20
        , El.centerX
        , El.centerY
        , El.width El.fill
        , Background.color bgColor
        ]
        (guessesBefore ++ (inputField :: guessesAfter))


rightArrow : El.Element msg
rightArrow =
    let
        svgArrow : Svg msg
        svgArrow =
            Svg.svg
                [ Svg.Attributes.height "45"
                , Svg.Attributes.viewBox "0 0 24 24"
                , Svg.Attributes.fill "none"
                , Svg.Attributes.stroke "currentColor"
                , Svg.Attributes.strokeWidth "1"
                ]
                [ Svg.path
                    [ Svg.Attributes.strokeLinecap "round"
                    , Svg.Attributes.strokeLinejoin "round"
                    , Svg.Attributes.d "M17 8l4 4m0 0l-4 4m4-4H3"
                    ]
                    []
                ]
    in
    El.html svgArrow


submitOnEnter : String -> Html.Attribute Msg
submitOnEnter content =
    Html.Events.on "keyup"
        (JD.field "code" JD.string
            |> JD.andThen
                (\keycode ->
                    case keycode of
                        "Enter" ->
                            JD.succeed <| SubmitString content

                        _ ->
                            JD.fail <| "ignoring keycode: " ++ keycode
                )
        )


viewInputField : Int -> String -> Bool -> El.Element Msg
viewInputField score content isValid =
    let
        scoreDisplay : El.Element Msg
        scoreDisplay =
            El.el
                [ El.width <| El.fillPortion 1
                , El.height El.fill
                , Border.color fgColor
                , Border.widthEach { top = 0, left = 0, right = 2, bottom = 0 }
                , Font.size 20
                ]
                (El.el [ El.centerX, El.centerY ]
                    (El.text <| String.fromInt score)
                )

        textInput : El.Element Msg
        textInput =
            Input.text
                [ El.width <| El.fillPortion 3
                , El.height El.shrink
                , Background.color bgColor
                , Border.width 0
                , Font.size 20
                , El.htmlAttribute <| submitOnEnter content
                , Input.focusedOnLoad
                ]
                { onChange = Change
                , text = content
                , placeholder = Nothing
                , label = Input.labelHidden "Guess the word"
                }

        submitButton : El.Element Msg
        submitButton =
            Input.button
                [ El.width <| El.fillPortion 1
                , El.height El.fill
                , El.centerY
                , Border.color fgColor
                , Border.widthEach { top = 0, left = 2, right = 0, bottom = 0 }
                ]
                { onPress =
                    if isValid then
                        Just (SubmitString content)

                    else
                        Nothing
                , label = rightArrow
                }
    in
    El.row
        [ Border.width 2
        , Border.color fgColor
        , Border.rounded 10
        , El.centerX
        , Font.size 30
        , El.width (El.fill |> El.maximum 420)
        , El.height El.shrink
        ]
        [ scoreDisplay
        , textInput
        , submitButton
        ]


type Direction
    = Before
    | After


{-| Elm-ui view where the guesses fade as their distance from the correct word grows
-}
viewGuesses : Direction -> List String -> String -> List (El.Element Msg)
viewGuesses dir guesses secret =
    let
        ( filterFun, reverseFun ) =
            case dir of
                Before ->
                    ( (>) secret, identity )

                After ->
                    ( (<) secret, List.reverse )

        -- function to calculate alpha values based on index in list
        alphaFun : Int -> Float
        alphaFun index =
            1 / (2 ^ toFloat (numGuessesShown - index - 1))

        -- actual alpha values used
        alphaAmounts : List Float
        alphaAmounts =
            List.map
                alphaFun
                (List.range 0 (numGuessesShown - 1) |> reverseFun)

        -- the `numGuessesShown` closest guesses
        nClosestGuesses : List String
        nClosestGuesses =
            guesses
                -- get only one side of secret
                |> List.filter filterFun
                -- lexicographical sort
                |> List.sort
                |> (\sorted ->
                        case dir of
                            Before ->
                                -- prepend spaces, take last N
                                (List.repeat numGuessesShown " " ++ sorted)
                                    |> List.reverse
                                    |> List.take numGuessesShown
                                    |> List.reverse

                            After ->
                                -- append spaces, take first N
                                (sorted ++ List.repeat numGuessesShown " ")
                                    |> List.take numGuessesShown
                   )
                -- truncate to show max `numGuessesShown` elements
                |> List.take numGuessesShown

        -- render html from string and alpha amount
        viewGuess : String -> Float -> El.Element msg
        viewGuess guess alpha =
            let
                color =
                    El.toRgb fgColor |> (\c -> El.rgba c.red c.green c.blue alpha)
            in
            El.el
                [ Font.size 30
                , El.centerX
                , Font.color color
                ]
                (El.text guess)
    in
    List.map2
        (\alpha guess -> viewGuess guess alpha)
        alphaAmounts
        nClosestGuesses



-- WIN SCREEN


{-| Screen to be displayed after the current day's word has been guessed
-}
viewWinScreen : Model -> El.Element msg
viewWinScreen model =
    let
        guessCount : String
        guessCount =
            "in "
                ++ (case List.length (todaysGuesses model) of
                        1 ->
                            "1 guess"

                        i ->
                            String.fromInt i ++ " guesses"
                   )

        countdownStr : String
        countdownStr =
            model.now
                |> Maybe.map countdown
                |> Maybe.withDefault ""

        statElem : String -> String -> El.Element msg
        statElem name text =
            El.column [ El.spacing 10, El.width El.fill ]
                [ El.el [ Font.size 40, El.centerX ] <| El.text text
                , El.el [ Font.size 20, Font.heavy, El.centerX ] <| El.text name
                ]

        streakElem : El.Element msg
        streakElem =
            model.now
                |> Maybe.map daysSince1970
                |> Maybe.map (streak model.persistedData)
                |> Maybe.map String.fromInt
                |> Maybe.map (statElem "STREAK")
                |> Maybe.withDefault El.none

        avgElem : El.Element msg
        avgElem =
            averageGuesses model.persistedData
                |> (\f -> minimumBy String.length [ Round.round 1 f, String.fromFloat f ])
                |> Maybe.withDefault "0"
                |> statElem "AVERAGE"
    in
    El.column
        [ El.centerX
        , El.centerY
        , El.spacing 30
        , Font.size 40
        ]
        [ El.column [ El.centerX ]
            [ El.el [ El.centerX, Font.heavy ] <| El.text "YOU HAVE WON"
            , El.el [ El.centerX ] <| El.text guessCount
            ]
        , El.row [ El.centerX, El.width El.fill ] [ streakElem, avgElem ]
        , guessesChart model.persistedData
        , El.el [] El.none -- super hacky way of adding bottom margin to chart
        , El.column [ El.centerX ]
            [ El.el [ El.centerX, Font.size 20, Font.heavy ] <| El.text "NEXT WORD IN"
            , El.el [ El.centerX ] <| El.text countdownStr
            ]
        ]


{-| A string with the human time remaining until next word
(Time until UTC midnight)
-}
countdown : Time.Posix -> String
countdown now =
    let
        hours =
            now
                |> Time.toHour Time.utc
                |> (\t -> 23 - t)
                |> String.fromInt

        minutes =
            now
                |> Time.toMinute Time.utc
                |> (\t -> 59 - t)
                |> String.fromInt
                |> String.pad 2 '0'

        seconds =
            now
                |> Time.toSecond Time.utc
                |> (\t -> 59 - t)
                |> String.fromInt
                |> String.pad 2 '0'
    in
    hours ++ ":" ++ minutes ++ ":" ++ seconds


{-| Preprocess list of guesses for each day, to more easily plot a histogram of guess counts.
-}
makePlotData : PersistedData -> List { guessCount : Int, wins : Int }
makePlotData data =
    let
        -- dict of (guessCount, number of wins)
        countDict : Dict Int Int
        countDict =
            data
                |> Dict.filter (\dayNumber guesses -> hasWonDay dayNumber data)
                |> Dict.foldl
                    (\dayNumber guesses newDict ->
                        Dict.insert
                            (List.length guesses)
                            (Dict.get (List.length guesses) newDict
                                |> Maybe.withDefault 0
                                |> (+) 1
                            )
                            newDict
                    )
                    Dict.empty
    in
    Maybe.map2
        (\min max ->
            List.map
                (\guessCount ->
                    { guessCount = guessCount
                    , wins = Dict.get guessCount countDict |> Maybe.withDefault 0
                    }
                )
                (List.range min max)
        )
        (Dict.keys countDict |> List.minimum)
        (Dict.keys countDict |> List.maximum)
        |> Maybe.withDefault []


guessesChart : PersistedData -> El.Element msg
guessesChart data =
    let
        width =
            300

        height =
            200

        plotData : List { guessCount : Int, wins : Int }
        plotData =
            makePlotData data

        fgColorStr : String
        fgColorStr =
            El.toRgb fgColor
                |> (\rgb ->
                        "rgb("
                            ++ String.fromInt (round (rgb.red * 255))
                            ++ ", "
                            ++ String.fromInt (round (rgb.green * 255))
                            ++ ", "
                            ++ String.fromInt (round (rgb.blue * 255))
                            ++ ", "
                            ++ String.fromInt (round (rgb.alpha * 255))
                            ++ ")"
                   )
    in
    El.el
        [ El.width (El.px width)
        , El.height (El.px height)
        , Font.size 10
        , El.centerX
        ]
    <|
        El.html <|
            C.chart
                [ CA.height height, CA.width width ]
                [ C.binLabels (.guessCount >> String.fromInt) [ CA.moveDown 20 ]
                , C.yLabels [ CA.ints ]
                , C.bars
                    []
                    [ C.bar
                        (.wins >> toFloat)
                        [ CA.roundTop 0.2
                        , CA.color fgColorStr
                        ]
                    ]
                    plotData
                ]
